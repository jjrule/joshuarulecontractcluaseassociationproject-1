public class MyContractController {
    
    public boolean displayPopup {get; set;} 
    public boolean displayPopupContract {get;set;}
    public Contract newContract {get;set;}
    public Contract currentContract {get;set;}
    public ContractClauseAssociation__c newClause {get;set;}
    public ID iden {get;set;}
    
    /**
     * Pulls the current contract records based on the id provided when contractDetails was called
     */
    public Contract getContract() {
        try{
        	currentContract = [select AccountId, Name, EndDate, StartDate, Status, ContractTerm from Contract
                 where id = :iden];
        } catch(System.QueryException e){
            System.debug(e.getMessage());
        }
        return currentContract;
    } 
    
    /**
     * Used to generate a list of all contracts ordered by their contract number
     * @return list of all contracts
     */
    public List<Contract> getContracts () {
        List<Contract> results;
        try{
            results = [Select id, contractNumber, AccountID, name FROM Contract order by contractNumber]; 
        } catch(System.QueryException e){
            System.debug(e.getMessage());
        }
        return results;
    }
        
    /**
     * Retrieves all the clauses associated with the current contract being viewed.
     * @return List of clause junction objects
     */
    public List<ContractClauseAssociation__c> getAssociations (){
        List<ContractClauseAssociation__c> ret;
        try{
            ret = [select Name, clause__r.name, clause__r.Description__c, clause__r.id, contract__r.name, id from ContractClauseAssociation__c where contract__r.id =: iden];
        } catch(System.QueryException e){
            System.debug(e.getMessage());
        }
        return ret;
    }
    
    /**
     * refresh the contract details page
     */
    public PageReference refreshContract (){
        return new PageReference('/apex/ContractEdit?id=' + iden);
    }
    
    /**
     * Navigate the to out of box edit page for clauses
     */
    public PageReference redirectToClauseEdit(){
        ID clauseID = ApexPages.currentPage().getParameters().get('ClauseAssociationID');
        return new PageReference('/' + clauseID);
    }
    
    /**
     * Redirects from the contract listing page to the details page. When preformed the id of 
     * the selected contract is stored into the iden variable.
     * @return a page reference to correspond to the new contract page
     */
	public PageReference contractDetails(){
        	iden = ApexPages.currentPage().getParameters().get('associatedID');
            PageReference pageRef = new PageReference('/apex/ContractEdit?id=' + iden);
            return pageRef;
	}
    
    /**
     * Navigate back to the landing page
     */
    public PageReference goBack(){
        PageReference pageRef = new PageReference('/apex/ContractList');
            return pageRef;
    }
    
    /**
     * Removes the maping of the clause to this contract by deleting the corresponding junction object.
     * @param ID of the junction variable
     * @return Refresh of the page with the current contract ID stored in the url.
     */ 
    public PageReference removeClause (){
        ID clauseAssocID = ApexPages.currentPage().getParameters().get('ClauseAssociationID');
       	ContractClauseAssociation__c del;
        try{
			del = [select id from ContractClauseAssociation__c where id =: clauseAssocID ];
        	delete del;
        } catch(System.QueryException e ){
            System.debug(e.getMessage());
        }
        return new PageReference('/apex/ContractEdit?id=' + iden);
    }
    
    /**
     * Remove the contract related to the contract association id.
     */
    public PageReference removeContract(){
        ID contractAssocID = ApexPages.currentPage().getParameters().get('ContractAssociationID');
        try{
            Contract del = [select id from Contract where id =: contractAssocID ];
        	delete del;
        }catch(System.QueryException e){
            System.debug(e.getMessage());
        }
        return new PageReference('/apex/ContractList');
    }

    /* Begin Popup Controls*/
    
    /**
     * Used for canceling a new clause creation. When called this method
     * will hide the popup
     */
    public void closePopupNewClause() {        
        displayPopup = false;    
    } 

    /**
     * Will close the popup window and save the details entered within
     * If the value for the new clause is null the window will instead just close
     * 
     * @return Refresh of the page with the current contract ID stored in the url.
     */
	public PageReference closePopupAndSaveNewClause() { 
        if (newClause != null){
            try{
            	insert newClause;
            }catch (System.DMLException e){
                System.debug(e.getMessage());
            }
        }
        
        displayPopup = false;
        return new PageReference('/apex/ContractEdit?id=' + iden);
    }
    
    /**
     * Will show the popup for adding new clauses to a contract.
     * The CCA 
     */
    public void showPopupNewClause() {        
        displayPopup = true;
        newClause = new ContractClauseAssociation__c ();
        newClause.contract__c = iden;
        //Will auto-complete the clause with the first in the clause__c list. This is an optional feature
        /*
        Clause__c cl = [Select id from Clause__c limit 1];
        CCA.Clause__c = cl.id;
        //*/
    }
    
    /**
     * Display the pop up window for the new contract as
     * well as create the contract object for future insertion.
     */
    public void showPopupNewContract(){
        newContract = new Contract();
        displayPopupContract = true;
    }
    
    /**
     * Close the popup window without saving
     */
    public void closePopupNewContract(){
        displayPopupContract = false;
    }
    
    /**
     * Close the popup window with saveing the records inputed.
     * @return a new page reference in order to force a refresh.
     */
    public PageReference closePopupNewContractSave(){
        if (newContract != null){
            try{
                newContract.Status = 'Draft';
            	insert newContract;
            }catch (System.DMLException e){
                System.debug(e.getMessage());
            }
        }
        displayPopupContract = false;
        return new PageReference('/apex/ContractList');
    }
}