public class TestSupportObjects {
    
    public Contract createContract (boolean toInsert){
        Contract ret = new Contract();
        ret.name = 'TestContract';
        ret.AccountId = createAccount(true).id;
        ret.StartDate = Date.newInstance(2018, 10, 25);
        ret.ContractTerm = 2;
        if (toInsert){
            insert ret;
        }
        return ret;
    }
    
    public Contract createBadContract (){
        Contract ret = new Contract();
        ret.name = 'TestContract';
        ret.AccountId = createAccount(false).id;
        ret.StartDate = Date.newInstance(2018, 10, 25);
        ret.ContractTerm = 2;
        return ret;
    }
    
    public Account createAccount(boolean toInsert){
     	Account ret = new Account();
        ret.name = 'DataFacortory';
        if (toInsert){
            insert ret;
        }
        return ret;
    }
    
    public Clause__c createClause(boolean toInsert){
        Clause__c ret = new Clause__c();
        ret.Type__c = 'Financial';
        if (toInsert){
            insert ret;
        }
        return ret;
    }
    
    public ContractClauseAssociation__c createAssociation (Contract con, Clause__c cla, boolean toInsert){
        ContractClauseAssociation__c ret = new ContractClauseAssociation__c();
        ret.Clause__c = cla.id;
        ret.Contract__c = con.id;
        if (toInsert){
            insert ret;
        }
        return ret;
    }
}