@isTest(seealldata = false) // we do not wish to use the data from the current database but instead use our own
public class MyContractContollerTest {
    
    /**
     * Test Functionality of the contract details function by creating a 
     * dummy contract and clause association and attempting to pull up the
     * contract detail page. Returning a page with a valid id error free
     * passes the test.
     */
    @isTest
    public static void testContractDetails (){
        TestSupportObjects creater = new TestSupportObjects();
        //create test data
        Contract testContract = creater.createContract(true);
        Clause__c testClause = creater.createClause(true);
       	ContractClauseAssociation__c cca = creater.createAssociation(testContract, testClause, true);
        Test.startTest();
        	ApexPages.currentPage().getParameters().put('associatedID', String.valueOf(testContract.Id));
			MyContractController  testContractController = new MyContractController();
        	PageReference ret = testContractController.contractDetails();
        	PageReference correct = new PageReference('/apex/ContractEdit?id=' + testContract.Id);
        	System.assertEquals(correct.getUrl(), ret.getUrl());
        Test.stopTest();
    }
    
    /**
     * Test to assure that the popup boolean value is properally being switched to true.
     */
    @isTest
    public static void testShowPopup (){
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	testContractController.showPopupNewClause();
        	System.assert(testContractController.displayPopup);
        Test.stopTest();
    }
    
    /**
     * Test to assure that the popup boolean value is properally being switched to false.
     */
    @isTest
    public static void testClosePopup (){
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	testContractController.closePopupNewClause();
        	System.assert(!testContractController.displayPopup);
        Test.stopTest();
    }
    
    /**
     * Test to assure that clause associations are properly removed from the contract. A 
     * contract is created with a clause association. Removal is then preformed followed by
     * a check on the clause associations table for the corrosponding id. If the query throws 
     * an error then the association was deleted.
     */
   	@isTest
    public static void testRemoveClause(){
        TestSupportObjects creater = new TestSupportObjects();
        //create test data
        Contract testContract = creater.createContract(true);
        Clause__c testClause = creater.createClause(true);
        ContractClauseAssociation__c cca = creater.createAssociation(testContract, testClause, true);
        Test.startTest();
        	//load the id for the contract page
        	ApexPages.currentPage().getParameters().put('associatedID', String.valueOf(testContract.Id));
			MyContractController testContractController = new MyContractController();
        	PageReference ret = testContractController.contractDetails();
        	//remove the clause association object
        	ApexPages.currentPage().getParameters().put('ClauseAssociationID', String.valueOf(cca.Id));
			ret = testContractController.removeClause();
        	//make sure the page is correct after removal
        	PageReference correct = new PageReference('/apex/ContractEdit?id=' + testContract.Id);
        	//check for removal
        	boolean passed = false;
        	try{
        		ContractClauseAssociation__c testResult = [Select id from ContractClauseAssociation__c where id =: cca.Id];
            }catch(System.QueryException e){
                passed = true; // exception means the object was properly deleted
            } 
        	System.assert(passed);
        	System.assertEquals(ret.getUrl(), correct.getUrl());
        Test.stopTest();
    }
    
    /**
     * Test to assure removing a non existing clause doesn't cause failure.
     */
    @isTest
    public static void testRemoveClauseInvalid(){
        TestSupportObjects creater = new TestSupportObjects();
        //create test data
        Contract testContract = creater.createContract(true);
        Clause__c testClause = creater.createClause(true);
        ContractClauseAssociation__c cca = creater.createAssociation(testContract, testClause, false);
        Test.startTest();
        	//load the id for the contract page
        	ApexPages.currentPage().getParameters().put('associatedID', String.valueOf(testContract.Id));
			MyContractController testContractController = new MyContractController();
        	PageReference ret = testContractController.contractDetails();
        	//remove the clause association object
        	ApexPages.currentPage().getParameters().put('ClauseAssociationID', String.valueOf(cca.Id));
			ret = testContractController.removeClause();
        	//make sure the page is correct after removal
        	PageReference correct = new PageReference('/apex/ContractEdit?id=' + testContract.Id);
        	System.assertEquals(ret.getUrl(), correct.getUrl());
        Test.stopTest();
    }
    
    /**
     * Test to assure removing null
     */
    @isTest
    public static void testRemoveClauseNull(){
        TestSupportObjects creater = new TestSupportObjects();
        //create test data
        Contract testContract = creater.createContract(true);
        Test.startTest();
        	//load the id for the contract page
        	ApexPages.currentPage().getParameters().put('associatedID', String.valueOf(testContract.Id));
			MyContractController testContractController = new MyContractController();
        	PageReference ret = testContractController.contractDetails();
        	//remove the clause association object
        	ApexPages.currentPage().getParameters().put('ClauseAssociationID', null);
			ret = testContractController.removeClause();
        	//make sure the page is correct after attempted removal
        	PageReference correct = new PageReference('/apex/ContractEdit?id=' + testContract.Id);
        	System.assertEquals(ret.getUrl(), correct.getUrl());
        Test.stopTest();
    }
    
    /**
     * Checks the returned list of getClauses to assure that only the clauses related
     * to the current contract are being pulled.
     */
    @isTest
    public static void testGetClauses(){
        TestSupportObjects creater = new TestSupportObjects();
        //Create test data
        Contract testContract = creater.createContract(true);
        Contract testContract2 = creater.createContract(true);
        Clause__c testClause1 = creater.createClause(true);
        Clause__c testClause2 = creater.createClause(true);
        ContractClauseAssociation__c cca1 = creater.createAssociation(testContract,testClause1, true);
        ContractClauseAssociation__c cca2 = creater.createAssociation(testContract,testClause2, true);
        ContractClauseAssociation__c cca3 = creater.createAssociation(testContract2,testClause2, true);
        
        
        Test.startTest();
        	ApexPages.currentPage().getParameters().put('associatedID', String.valueOf(testContract.Id));
			MyContractController testContractController = new MyContractController();
        	PageReference ret = testContractController.contractDetails();
        	List <ContractClauseAssociation__c> clauseList = testContractController.getAssociations();
        	System.assert(clauseList.size() == 2);
        Test.stopTest();
    }
    
     /**
     * Checks the returned list of getClauses to assure that only the clauses related
     * to the current contract are being pulled.
     */
    @isTest
    public static void testGetClausesInvalid(){
        TestSupportObjects creater = new TestSupportObjects();
        //Create test data
        Contract testContract = creater.createContract(false);
        Contract testContract2 = creater.createContract(false);
        Clause__c testClause1 = creater.createClause(false);
        Clause__c testClause2 = creater.createClause(false);
        ContractClauseAssociation__c cca1 = creater.createAssociation(testContract,testClause1, false);
        ContractClauseAssociation__c cca2 = creater.createAssociation(testContract,testClause2, false);
        ContractClauseAssociation__c cca3 = creater.createAssociation(testContract2,testClause2, false);
        Test.startTest();
        	ApexPages.currentPage().getParameters().put('associatedID', String.valueOf(testContract.Id));
			MyContractController testContractController = new MyContractController();
        	PageReference ret = testContractController.contractDetails();
        	List <ContractClauseAssociation__c> clauseList = testContractController.getAssociations();
        	System.assert(clauseList.size() == 0);
        Test.stopTest();
    }
    
    /**
     * Test the go back function to assure that the page reference being returned is correct.
     * The reference returned should always navigate to the main page since there are only two pages.
     */
    @isTest
    public static void testGoBack(){
        Test.startTest();
			MyContractController  testContractController = new MyContractController();
        	PageReference ret = testContractController.goBack();
        	PageReference correct = new PageReference('/apex/ContractList?');
        	System.assertEquals(correct.getUrl(), ret.getUrl());
        Test.stopTest();
    }
    
    /**
     * Test to assure all contracts are being listed on the main page.
     */
    @isTest
    public static void testGetContracts(){
        TestSupportObjects creater = new TestSupportObjects();
        Contract con1 = creater.createContract(true);
        Contract con2 = creater.createContract(true);
        Contract con3 = creater.createContract(true);
        Contract con4 = creater.createContract(true);
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	List<Contract> ret = testContractController.getContracts();
        	System.assert(ret.size() == 4);
        Test.stopTest();
    }
    
     /**
     * Test to assure getContracts doesn't break when database is empty
     */
    @isTest
    public static void testGetContractsInvalid(){
        TestSupportObjects creater = new TestSupportObjects();
        Contract con1 = creater.createContract(false);
        Contract con2 = creater.createContract(false);
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	List<Contract> ret = testContractController.getContracts();
        	System.assert(ret.size() == 0);
        Test.stopTest();
    }
    
    /**
     * Test to assure that the correct contract is being pulled by getContract.
     */
    @isTest
    public static void testGetContract(){
    	TestSupportObjects creater = new TestSupportObjects();
        Contract con1 = creater.createContract(true);
        Contract con2 = creater.createContract(true);
        Contract con3 = creater.createContract(true);
        Contract con4 = creater.createContract(true);
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	ApexPages.currentPage().getParameters().put('associatedID', String.valueOf(con2.Id));
        	testContractController.contractDetails();
        	Contract ret = testContractController.getContract();
        	System.assert(ret.id == con2.id);
        Test.stopTest();
    }
    
    /**
     * Test to assure that querying a non existant contract catches the error without failure.
     */
    @isTest
    public static void testGetContractInvalid(){
    	TestSupportObjects creater = new TestSupportObjects();
        Contract con1 = creater.createContract(true);
        Contract con2 = creater.createContract(false);
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	ApexPages.currentPage().getParameters().put('associatedID', String.valueOf(con2.Id));
        	testContractController.contractDetails();
        	Contract ret = testContractController.getContract();
        	boolean passed = false;
            if (ret == null){
                passed = true;
            }
        	System.assert(passed);
        Test.stopTest();
    }
    
    /**
     * Test to assure that saving a new clause association works properly.
     * A new clause association is created and assigned to the controller's
     * newClause variable. Then a save is performed. The page reference,
     * closing of the popup window, and the insertion are all tested.
     */
    @isTest
    public static void testClosePopupAndSave(){
        TestSupportObjects creater = new TestSupportObjects();
        Contract con = creater.createContract(true);
        Clause__c cla = creater.createClause(true);
        ContractClauseAssociation__c cca = creater.createAssociation(con,cla, false);
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	ApexPages.currentPage().getParameters().put('associatedID', String.valueOf(con.Id));
        	testContractController.contractDetails();
        	//instatiate the newClause variable
        	testContractController.showPopupNewClause();
        	testContractController.newClause = cca;
        	PageReference page = testContractController.closePopupAndSaveNewClause();
        	//flag to track if insertion was succesful
        	boolean passed = true;        	
        	try{
        		ContractClauseAssociation__c ret = [select id from ContractClauseAssociation__c where id=: cca.id];
            }catch(System.QueryException e){
                passed = false;
            }
        	PageReference correct = new PageReference('/apex/ContractEdit?id=' + con.id);
        	System.assert(passed);
        	System.assertEquals(correct.getUrl(), page.getUrl());
        	System.assert(!testContractController.displayPopup);
        Test.stopTest();
    }
    
    
    /**
     * Test the controller's save method to assure that a null value does
     * not create an error.
     */
    @isTest
    public static void testClosePopupAndSaveWithNull(){
        TestSupportObjects creater = new TestSupportObjects();
        Contract con = creater.createContract(false);
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	ApexPages.currentPage().getParameters().put('associatedID', String.valueOf(con.Id));
        	testContractController.contractDetails();
        	//instatiate the newClause variable
        	testContractController.showPopupNewClause();
        	testContractController.newClause = null;
        	PageReference page = testContractController.closePopupAndSaveNewClause();
        	PageReference correct = new PageReference('/apex/ContractEdit?id=' + con.id);
        	System.assertEquals(correct.getUrl(), page.getUrl());
        	System.assert(!testContractController.displayPopup);
    }
    
    /**
     * Test the insertion with an association value that will trigger a 
     * DML exception.
     */
    @isTest
    public static void testClosePopupAndSaveWithInvalidInsert(){
        TestSupportObjects creater = new TestSupportObjects();
        Contract con = creater.createContract(false);
        Clause__c cla = creater.createClause(false);
        ContractClauseAssociation__c cca = creater.createAssociation(con,cla, false);
        cca.Clause__c = null;
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	ApexPages.currentPage().getParameters().put('associatedID', String.valueOf(con.Id));
        	testContractController.contractDetails();
        	//instatiate the newClause variable
        	testContractController.showPopupNewClause();
        	testContractController.newClause = cca;
        	PageReference page = testContractController.closePopupAndSaveNewClause();
        	PageReference correct = new PageReference('/apex/ContractEdit?id=' + con.id);
        	System.assertEquals(correct.getUrl(), page.getUrl());
        	System.assert(!testContractController.displayPopup);
    }
    /**
     * Check that under ideal conditions that the selected contract is removed
     */
    @isTest
    public static void testRemoveContract(){
        TestSupportObjects creater = new TestSupportObjects();
        Contract con1 = creater.createContract(true);
        Contract con2 = creater.createContract(true);
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	ApexPages.currentPage().getParameters().put('ContractAssociationID', String.valueOf(con1.Id));
       		//remove the contract 1 and check the returned page value
        	PageReference ret = testContractController.removeContract();
        	PageReference correct = new PageReference('/apex/ContractList');
        	//check for removal
        	boolean passed = false;
        	try{
        		Contract testResult = [Select id from Contract where id =: con1.Id];
            }catch(System.QueryException e){
                passed = true; // exception means the object was properly deleted
            } 
        	System.assert(passed);
        	System.assertEquals(ret.getUrl(), correct.getUrl());
        Test.stopTest();
    }
        
    /**
     * Test to remove a contract that doesn't exist in the database,
     * Should throw and error and trigger the try catch.
     */
    @isTest
    public static void testRemoveContractInvalid(){
        TestSupportObjects creater = new TestSupportObjects();
        //create data needed
        Contract con1 = creater.createContract(false);
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	ApexPages.currentPage().getParameters().put('ContractAssociationID', String.valueOf(con1.Id));
       		//remove the contract 1 and check the returned page value
        	PageReference ret = testContractController.removeContract();
        	PageReference correct = new PageReference('/apex/ContractList');
        	//assure that the url returned is still valid despite the failed opperation
        	System.assertEquals(ret.getUrl(), correct.getUrl());
        Test.stopTest();
    }
    
    /**
     * Test to assure that when selecting the clause edit function that the redirect goes 
     * to the correct clause
     */
    @isTest
    public static void testRedirectToClauseEdit (){
        TestSupportObjects creater = new TestSupportObjects();
        //create data needed
        Contract con = creater.createContract(true);
        Clause__c cla = creater.createClause(true);
        Clause__c bad = creater.createClause(true);
        ContractClauseAssociation__c cca = creater.createAssociation(con, cla, true);
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	ApexPages.currentPage().getParameters().put('ClauseAssociationID', String.valueOf(cla.Id));
        	PageReference ret = testContractController.redirectToClauseEdit();
        	PageReference correct= new PageReference('/' + cla.id);
        	System.assertEquals(correct.getUrl(), ret.getUrl());
        Test.stopTest();
    }
    
    /**
     * Test to assure that the popup boolean is turned to true so the popupup
     * window for new contract appears when rerendered.
     */
    @isTest
    public static void testShowPopupNewContract (){
         Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	testContractController.showPopupNewContract();
        	System.assert(testContractController.displayPopupContract);
        Test.stopTest();
    }
    
    /**
     * Test to assure that to popup window for new contract closes properly
     * by turning the its boolean to false.
     */
    @isTest
    public static void testClosePopupNewContract(){
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	testContractController.closePopupNewContract();
        	System.assert(!testContractController.displayPopupContract);
        Test.stopTest();
    }
    
    /**
     * Test to assure that a new contract entered withing the popup is save properly.
     */
    @isTest
    public static void testClosePopupNewContractSave(){
		TestSupportObjects creater = new TestSupportObjects();   
        Contract con = creater.createContract(false);
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	testContractController.showPopupNewContract();
        	testContractController.newContract = con;
        	testContractController.closePopupNewContractSave();
        	//preform test to assure data was inserted
        	boolean passed = true;
            try{
                Contract ret = [select id from Contract where id =: con.id];
            }catch(System.QueryException e){
                passed = false;
            }
        	System.assert(passed);
        	System.assert(!testContractController.displayPopupContract);
        
        Test.stopTest();
    }
    
    /**
     * Test to ensure that invalid data is not put into the database.
     */
    @isTest
    public static void testClosePopupNewContractSaveInvalid(){
		TestSupportObjects creater = new TestSupportObjects();     
        Contract con = creater.createBadContract();
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	testContractController.showPopupNewContract();
        	testContractController.newContract = con;
        	testContractController.closePopupNewContractSave();
        	//preform test to assure data was inserted
        	boolean passed = true;
            try{
                Contract ret = [select id from Contract where id =: con.id];
            }catch(System.QueryException e){
                passed = false;
            }
        	System.assert(!passed);
        	System.assert(!testContractController.displayPopupContract);
        Test.stopTest();
    }
    
    /**
     * Test that to assure that the contract page refreshing method works
     */
    @isTest
    public static void testRefreshContract(){
        TestSupportObjects creater = new TestSupportObjects();    
        Contract con = creater.createContract(true);
        Test.startTest();
        	MyContractController  testContractController = new MyContractController();
        	ApexPages.currentPage().getParameters().put('associatedID', String.valueOf(con.Id));
        	testContractController.contractDetails();
        	PageReference ret = testContractController.refreshContract();
        	PageReference correct = new PageReference('/apex/ContractEdit?id=' + con.id);
        	System.assertEquals(ret.getUrl(), correct.getUrl());
        Test.stopTest();
    }
}